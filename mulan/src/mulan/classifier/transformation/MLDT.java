package mulan.classifier.transformation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import mulan.classifier.MultiLabelLearner;
import mulan.classifier.MultiLabelOutput;
import mulan.data.MultiLabelInstances;
import weka.core.Instance;
import weka.core.Instances;

/**
 * Prediction is made using each matrix value as is originally.
 *
 * @author Victor
 */
public class MLDT extends TransformationBasedMultiLabelLearner {

    private static final long serialVersionUID = -446512578928821388L;
    protected int num_attrs_submitted = 0;
    private int seed = 1;
    private MultiLabelLearner[] classifiers;
    private int classifiersLength = 2;
    private MLDTBR[] DT;
    private boolean buildEach = true;
    private boolean debug = false;
    private double confThreshold = 0.5;
    private PredictionType prediction;
    private boolean useMedian = false;

    /**
     * The supported prediction types. For individual prediction each matrix is
     * used as it is. For combined prediction the values of the complementary
     * labels are combined in one matrix column.
     */
    public enum PredictionType {
        INDIVIDUAL, COMBINED;
    }

    public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public void setDebug(boolean enableDebug) {
        debug = enableDebug;
    }

    double mstime(long t) {
        return (System.nanoTime() - t) / 1e6;
    }

    public void setSeed(int seed) {
        this.seed = seed;
    }

    public void setBuilEach(boolean b) {
        this.buildEach = b;
    }

    /**
     * Creates a new instance
     *
     * @param classifiers the base-level classification algorithm that will be
     * used for training each of the binary models
     * @param aChain
     */
    public MLDT(MultiLabelLearner[] classifiers, PredictionType type) {
        super();
        this.classifiers = classifiers;
        this.classifiersLength = classifiers.length;
        this.prediction = type;
    }

    private void buildTemplates(MultiLabelInstances mlinstances) throws Exception {
        DT = new MLDTBR[numLabels];
        for (int i = 0; i < numLabels; i++) {
            DT[i] = new MLDTBR();
        }

        Instances newtrainData = mlinstances.getDataSet();
        for (int d = 0; d < newtrainData.size(); d++) {
            Instance inst = newtrainData.get(d);
            boolean[] trueLabels = getTrueLabels(inst, numLabels, mlinstances.getLabelIndices());

            for (int j = 0; j < classifiersLength; j++) {
                double[] confidencesForInstance = classifiers[j].makePrediction(inst).getConfidences();

                if (debug) {
                    System.out.println("Original instance:");
                    System.out.println(Arrays.toString(inst.toDoubleArray()));
                }

                for (int i = 0; i < numLabels; i++) {
                    DT[i].addColumn(j, confidencesForInstance, trueLabels[i]);
                }
            }

        }

        for (int i = 0; i < numLabels; i++) {
            if (debug) {
                System.out.println("Label #" + i);
            }
            DT[i].computeTemplates();
        }
    }

    private boolean[] getTrueLabels(Instance instance, int numLabels, int[] labelIndices) {

        boolean[] trueLabels = new boolean[numLabels];
        for (int counter = 0; counter < numLabels; counter++) {
            int classIdx = labelIndices[counter];
            String classValue = instance.attribute(classIdx).value((int) instance.value(classIdx));
            trueLabels[counter] = classValue.equals("1");
        }

        return trueLabels;
    }

    @Override
    protected void buildInternal(MultiLabelInstances all) throws Exception {
        //TODO: split the instances in train and validadion instances using the seed value
//        Instances dataSet = train.getDataSet();
//        Random randomNumberGenerator = dataSet.getRandomNumberGenerator(seed);  //Works?
//        randomNumberGenerator.setSeed(seed);                                    //Necessary?
//        dataSet.resample(randomNumberGenerator);                                //Overkill?
//
//        List<Instance> subList = dataSet.subList(0, dataSet.size()/2);
//        List<Instance> subList1 = dataSet.subList(dataSet.size()/2 + 1, dataSet.size());
        if (buildEach) {
            Instances dataSet = all.getDataSet();
            dataSet.resample(new Random(seed));
            double div = 0.5;
            Instances itrain = new Instances(dataSet, 0, (int) (dataSet.size() * div));
            Instances ivalidation = new Instances(dataSet, (int) (dataSet.size() * div), (int) (dataSet.size() - dataSet.size() * div));
            MultiLabelInstances mlTrain = new MultiLabelInstances(itrain,
                    all.getLabelsMetaData());
            MultiLabelInstances mlValidation = new MultiLabelInstances(ivalidation,
                    all.getLabelsMetaData());

            for (int i = 0; i < this.classifiersLength; i++) {
                classifiers[i].build(mlTrain);
            }

            buildTemplates(mlValidation);
        } else {
            buildTemplates(all);
        }

    }

    @Override
    protected MultiLabelOutput makePredictionInternal(Instance instance) throws Exception {
        double[][] confidencesMatrix = new double[classifiersLength][numLabels];
        boolean[] bipartition = new boolean[numLabels];

        for (int c = 0; c < classifiersLength; c++) {
            confidencesMatrix[c] = classifiers[c].makePrediction(instance).getConfidences();
        }

        if (this.prediction == PredictionType.INDIVIDUAL) {
            for (int i = 0; i < numLabels; i++) {
                bipartition[i] = DT[i].makePredictionIndividual(i, confidencesMatrix);
            }
        } else {
            for (int i = 0; i < numLabels; i++) {
                bipartition[i] = DT[i].makePredictionCombined(i, confidencesMatrix);
            }
        }

        return new MultiLabelOutput(bipartition);
    }

    public MultiLabelOutput makePrediction(double[][] confidencesMatrix) {
        boolean[] bipartition = new boolean[numLabels];
        if (this.prediction == PredictionType.INDIVIDUAL) {
            for (int i = 0; i < numLabels; i++) {
                bipartition[i] = DT[i].makePredictionIndividual(i, confidencesMatrix);
            }
        } else {
            for (int i = 0; i < numLabels; i++) {
                bipartition[i] = DT[i].makePredictionCombined(i, confidencesMatrix);
            }
        }

        return new MultiLabelOutput(bipartition);
    }

    /**
     * Multilabel Decision Template builder using the Binary Relevance approach
     * One for each label
     */
    private class MLDTBR {

        private int posivitesCount = 0;
        private int negativesCount = 0;
        private final double[][] positiveMatrix = new double[classifiersLength][numLabels];
        private final double[][] negativeMatrix = new double[classifiersLength][numLabels];
        private final ArrayList<double[]> positiveInstances = new ArrayList<>();
        private final ArrayList<double[]> nagetiveInstances = new ArrayList<>();

        public MLDTBR() {
        }

        public void addColumn(int index, double[] confidences, boolean trueLabel) {
//            DEBUG
            if (debug) {
                System.out.println("ADD Column:" + index + ">Confidences: " + Arrays.toString(confidences) + "|" + trueLabel);
            }
            if (trueLabel) {
                //Add to positiveMatrix
                addMatrixMean(index, positiveMatrix, confidences);
                addInstances(positiveInstances, confidences);
                posivitesCount++;
            } else {
                //Add to negativeMatrix
                addMatrixMean(index, negativeMatrix, confidences);
                addInstances(nagetiveInstances, confidences);
                negativesCount++;
            }
        }

        public void computeTemplates() {
            posivitesCount = posivitesCount / classifiersLength;
            negativesCount = negativesCount / classifiersLength;

            if (debug) {
                System.out.println("Sum negatives: ");
                printMatrix(negativeMatrix);
                System.out.println("~~~~~~~~~~~~~~~~");
            }

            //compute positive matrix mean
            if (useMedian) {
                computeMatrixMedian(positiveMatrix, positiveInstances);
                computeMatrixMedian(negativeMatrix, nagetiveInstances);

            } else {
                computeMatrixMean(positiveMatrix, posivitesCount);
                computeMatrixMean(negativeMatrix, negativesCount);
            }

//            normalizeMatrixLines(positiveMatrix);
            //compute negative matrix mean
//            normalizeMatrixLines(negativeMatrix);
//          DEBUG
            if (debug) {
                System.out.println("Compute Templates");
                System.out.println("-------------");
                System.out.println("Positives: " + posivitesCount);
                printMatrix(positiveMatrix);
//                normalizeMatrixLines(positiveMatrix);
//                System.out.println("Normalized Positives: ");
//                printMatrix(positiveMatrix);
                System.out.println("Negative: " + negativesCount);
                printMatrix(negativeMatrix);
//                System.out.println("-------------");
//                System.out.println("Normalized Negatives: ");
//                printMatrix(negativeMatrix);
            }
        }

        private void computeMatrixMean(double[][] matrix, int count) {
            for (int i = 0; i < classifiersLength; i++) {
                for (int j = 0; j < numLabels; j++) {
                    matrix[i][j] /= count;
                }
            }
        }

        private void computeMatrixMedian(double[][] matrix, ArrayList<double[]> instances) {
            int len = classifiersLength;

            for (int j = 0; j < len; j++) {

                for (int l = 0; l < numLabels; l++) {
                    ArrayList<Double> allValuesL = new ArrayList<>();
                    for (int i = j; i < instances.size(); i += len) {
                        allValuesL.add(instances.get(i)[l]);
                    }
                    double median = median(allValuesL);
                    matrix[j][l] = median;
                    if (debug) {
                        System.out.println("computeMatrixMedian");
                        System.out.println(Arrays.toString(allValuesL.toArray()) + " >median:" + median);
                    }
                }

            }

        }

        private double median(ArrayList<Double> numArray) {

            Collections.sort(numArray);
            int middle = numArray.size() / 2;
            double medianValue = 0; //declare variable 
            if (numArray.size() % 2 == 1) {
                medianValue = numArray.get(middle);
            } else {
                medianValue = (numArray.get(middle - 1) + numArray.get(middle)) / 2;
            }

            return medianValue;
        }

        /**
         * Matrix normalization line by line. Each line has to sum 1
         *
         * @param matrix
         */
        private void normalizeMatrixLines(double[][] matrix) {
            for (int i = 0; i < classifiersLength; i++) {
                double lineSum = 0;
                for (int j = 0; j < numLabels; j++) {
                    lineSum += matrix[i][j];
                }

                for (int j = 0; j < numLabels; j++) {
                    matrix[i][j] /= lineSum;
                }
            }
        }

        private void addMatrixMean(int index, double[][] matrix, double[] confidences) {
            for (int j = 0; (j < numLabels) && (j < confidences.length); j++) { //FIXME: RAkEL
                matrix[index][j] += confidences[j];
            }
        }

        private void addInstances(ArrayList<double[]> instances, double[] confidences) {
            instances.add(confidences);
        }

        public boolean makePrediction(double[][] confidencesMatrix) {
            double euclidPositiveDist = 0;
            double euclidNegativeDist = 0;

            //DEBUG
            if (debug) {
                System.out.println("Prediction");
                printMatrix(confidencesMatrix);
            }
            for (int i = 0; i < classifiersLength; i++) {
                for (int j = 0; (j < numLabels) && (j < confidencesMatrix[i].length); j++) {//FIXME:RAkEL
                    euclidPositiveDist += Math.abs(confidencesMatrix[i][j] - positiveMatrix[i][j]);
                    euclidNegativeDist += Math.abs(confidencesMatrix[i][j] - negativeMatrix[i][j]);
                }
            }

            double conf = 1 - (euclidPositiveDist / (euclidPositiveDist + euclidNegativeDist));

//            System.out.println("PositiveDist:"+euclidPositiveDist);
//            System.out.println("NegativeDist:"+euclidNegativeDist);
            //TODO: computeDistance method for two matrix
            //TODO: create confidence based on the distance of the matrix (check RDBR-DT)
            //DEBUG
            if (debug) {
                boolean b = euclidPositiveDist < euclidNegativeDist;
                System.out.println("Result: " + b);
                System.out.println("Conf: " + conf);
            }
            return euclidPositiveDist < euclidNegativeDist;
//            return conf > confThreshold;
        }

        //INDIVIDUAL PREDICTION TYPE
        public boolean makePredictionIndividual(int index, double[][] confidencesMatrix) {
            double euclidPositiveDist = 0;
            double euclidNegativeDist = 0;

            for (int i = 0; i < classifiersLength; i++) {
                euclidPositiveDist += Math.abs(confidencesMatrix[i][index] - positiveMatrix[i][index]);
                euclidNegativeDist += Math.abs(confidencesMatrix[i][index] - negativeMatrix[i][index]);
            }
            double conf = 1 - (euclidPositiveDist / (euclidPositiveDist + euclidNegativeDist));
            //TODO: computeDistance method for two matrix
            //TODO: create confidence based on the distance of the matrix (check RDBR-DT)
            //DEBUG
            if (debug) {
                boolean b = euclidPositiveDist < euclidNegativeDist;
                System.out.println("Result: " + b);
                System.out.println("Conf: " + conf);
            }
            //TODO: computeDistance method for two matrix
            //TODO: create confidence based on the distance of the matrix (check RDBR-DT)
            return conf > confThreshold;
        }

        public boolean makePredictionCombined(int index, double[][] confidencesMatrix) {
            double euclidPositiveDist = 0;
            double euclidNegativeDist = 0;
            double[] othersNegativeDist = new double[classifiersLength];
            double[] othersPositiveDist = new double[classifiersLength];
            double[] originalMean = new double[classifiersLength];

            for (int c = 0; c < classifiersLength; c++) {
                for (int l = 0; l < numLabels; l++) {
                    if (l != index) {
                        othersPositiveDist[c] += positiveMatrix[c][l];
                        othersNegativeDist[c] += negativeMatrix[c][l];
                        originalMean[c] += confidencesMatrix[c][l];
                    }
                }
            }

            for (int c = 0; c < classifiersLength; c++) {
                othersPositiveDist[c] /= numLabels;
                othersNegativeDist[c] /= numLabels;
                originalMean[c] /= numLabels;
            }

            for (int i = 0; i < classifiersLength; i++) {
                euclidPositiveDist += Math.abs(confidencesMatrix[i][index] - positiveMatrix[i][index]);
                euclidPositiveDist += Math.abs(originalMean[i] - othersPositiveDist[i]);
                euclidNegativeDist += Math.abs(confidencesMatrix[i][index] - negativeMatrix[i][index]);
                euclidNegativeDist += Math.abs(originalMean[i] - othersNegativeDist[i]);
            }

            double conf = 1 - (euclidPositiveDist / (euclidPositiveDist + euclidNegativeDist));
            if (debug) {
                boolean b = euclidPositiveDist < euclidNegativeDist;
                System.out.println("Result: " + b);
                System.out.println("Conf: " + conf);
            }
            return conf > confThreshold;
        }

    }

    /*Debug only*/
    //c1->[d1,d2,...,dl]
    //c2->[d1,d2,...,dl]
    //c3->[d1,d2,...,dl]
    private void printMatrix(double[][] confidencesMatrix) {
        for (int i = 0; i < classifiersLength; i++) {
            System.out.println(Arrays.toString(confidencesMatrix[i]));
        }
        System.out.println("");
    }

}
