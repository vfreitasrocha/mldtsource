/*
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 */

/*
 *    MRLM.java
 *    Copyright (C) 2009-2012 UFES
 */
package mulan.classifier.transformation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import mulan.classifier.MultiLabelLearner;
import mulan.classifier.MultiLabelOutput;
import mulan.classifier.transformation.BinaryRelevance;
import mulan.classifier.transformation.TransformationBasedMultiLabelLearner;
import mulan.data.MultiLabelInstances;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;
import weka.filters.unsupervised.attribute.Remove;

public class MRLM extends TransformationBasedMultiLabelLearner {

	/**
	 * 
	 */
	private static final long serialVersionUID = -446512578928821388L;
	protected int num_attrs_submitted = 0;
	private int seed = 1;

	public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public MRLM() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

	double mstime(long t) {
		return (System.nanoTime() - t) / 1e6;
	}

	/**
	 * The ensemble of binary relevance models. These are Weka
	 * FilteredClassifier objects, where the filter corresponds to removing all
	 * label apart from the one that serves as a target for the corresponding
	 * model.
	 */
	protected FilteredClassifier[][] ensemble;
	private int chainSize = 2;

	public boolean isUseTrainPropag() {
		return useTrainPropag;
	}

	public void setSeed(int seed) {
		this.seed = seed;
	}

	public void setUseTrainPropag(boolean useTrainPropag) {
		this.useTrainPropag = useTrainPropag;
	}

	public boolean isInstanceSelection() {
		return false;
	}

	public boolean isUseOnlyLabels() {
		return useOnlyLabels;
	}

	public boolean isUseMirrorLabel() {
		return useMirrorLabel;
	}

	public boolean isUseConfiability() {
		return useConfiability;
	}

	public boolean isChainUpdate() {
		return chainUpdate;
	}

	private int realchainSize;
	MultiLabelLearner baseml;
	Add[] addsattr;
	double Climit = 0.1;

	List<Integer> indexs = new ArrayList<Integer>();
	private boolean useTrainPropag;
	private boolean useOnlyLabels;
	private boolean useMirrorLabel = true;
	private boolean useConfiability = true;
	private boolean chainUpdate = true;
	// private int max_c = -1;
	private List<Classifier> baseClassifs = new ArrayList<Classifier>();

	private boolean[] mlbase_bipar;
	private double[] mlbase_conf;
	private double[] bestconfs;
	public boolean useSubIfConf = false;
	private Instances trainData;
	private Instances newtrainData;
	private boolean backChain = false;
	private int maxevals;
	private int mlocombtype = 0;

	/**
	 * Creates a new instance
	 * 
	 * @param classifier
	 *            the base-level classification algorithm that will be used for
	 *            training each of the binary models
	 * @param aChain
	 */
	public MRLM(Classifier classifier, int chainSize) {
		super(classifier);
		this.chainSize = chainSize;
		realchainSize = chainSize;
		baseml = new BinaryRelevance(classifier);

		this.setTrainPropagation(false);
		this.setUseOnlyLabels(false);
		this.setUseMirrorLabel(false);
		this.setUseConfiability(false);
		this.setChainUpdate(true);
		this.setUseSubIfConf(false);
		this.setBackChain(false);
	}

	/**
	 * Creates a new instance
	 * 
	 * @param classifier
	 *            the base-level classification algorithm that will be used for
	 *            training each of the binary models
	 * @param aChain
	 */
	public MRLM(MultiLabelLearner baseml, Classifier classifier, int chainSize) {
		super(classifier);
		this.chainSize = chainSize;
		realchainSize = chainSize;
		this.baseml = baseml;

		this.setTrainPropagation(false);
		this.setUseOnlyLabels(false);
		this.setUseMirrorLabel(false);
		this.setUseConfiability(false);
		this.setChainUpdate(true);
		this.setUseSubIfConf(false);
		this.setBackChain(false);

	}

	// public MRLM(MultiLabelLearner baseml, List<Classifier> classifs, int
	// chainSize) {
	// super(classifs.get(0));
	// setBaseClassifiers(classifs);
	// this.chainSize = chainSize;
	// realchainSize = chainSize;
	// this.baseml = baseml;
	// useTrainPropag = false;
	// useOnlyLabels = false;
	// }

	private void TransformInstance(Instance inst, MultiLabelOutput mlo, boolean useConf) {
		if (mlo != null) {
			double[] confs = mlo.getConfidences();
			boolean[] bipart = mlo.getBipartition();
			for (int j = 0; j < numLabels; j++) {
				final int attr_index = inst.numAttributes() - numLabels + j;
				if (useConf) {
					inst.setValue(attr_index, confs[j]);
				} else {
					if ((!useSubIfConf) || (Math.abs(confs[j] - 0.5) >= Math.abs(bestconfs[j] - 0.5))) {
						inst.setValue(attr_index, bipart[j] ? 1 : 0);
						bestconfs[j] = confs[j];
					}
				}
			}
		}
	}

	private void TransformInstance(Instance inst, MultiLabelOutput mlo) {
		TransformInstance(inst, mlo, true);
	}

	private MultiLabelOutput ChainMakePrediction(int c, Instance inst, MultiLabelOutput prevOut) throws Exception {

		boolean[] bipartition = new boolean[numLabels];
		double[] confidences = new double[numLabels];
		List<Integer> chainOrder = new ArrayList<Integer>();

		for (int i = 0; i < numLabels; i++) {
			chainOrder.add(i);
		}

		// Collections.shuffle(chainOrder, new Random(seed));

		// if (c < realchainSize - 1) {
		TransformInstance(inst, prevOut, useConfiability);
		// } else {
		// TransformInstance(inst, prevOut, false);
		// }
		for (int i = 0; i < chainOrder.size(); i++) {
			int j = chainOrder.get(i);
			double[] distribution;
			distribution = ensemble[c][j].distributionForInstance(inst);

			int maxIndex = (distribution[0] > distribution[1]) ? 0 : 1;

			// Ensure correct predictions both for class values {0,1} and {1,0}
			bipartition[j] = maxIndex == 1;

			// The confidence of the label being equal to 1
			confidences[j] = distribution[1];

			if (inst.value((inst.numAttributes() - numLabels + j)) != maxIndex) {
				if (chainUpdate) {
					inst.setValue(inst.numAttributes() - numLabels + j, maxIndex);
				}

				if (backChain && (i > 0) && chainUpdate) {
					boolean[] prevBipart = prevOut.getBipartition();
					double[] prevConf = prevOut.getConfidences();
					for (int k = i + 1; k < chainOrder.size(); k++) {
						j = chainOrder.get(k);
						bipartition[j] = prevBipart[j];
						confidences[j] = prevConf[j];
					}
					break;
				}
			}

		}

		MultiLabelOutput mlo = new MultiLabelOutput(bipartition, confidences);
		return mlo;
	}

	private MultiLabelOutput ChainMakePrediction(int c, Instance inst) throws Exception {
		return ChainMakePrediction(c, inst, null);
	}

	public void setBaseClassifiers(List<Classifier> cs) {
		baseClassifs.addAll(cs);
	}

	private FilteredClassifier constructClassifier(int labeli, Instances trainDataset) throws Exception {
		FilteredClassifier fc = new FilteredClassifier();
		if (baseClassifs.isEmpty()) {
			fc.setClassifier(AbstractClassifier.makeCopy(baseClassifier));
		} else {
			fc.setClassifier(AbstractClassifier.makeCopy(baseClassifs.get(labeli)));
		}
		int[] indicesToRemove;
		int nattrs = trainDataset.numAttributes();
		// int[] indicesToRemove = new int[nattrs-numLabels];
		// ensemble[c][j].setClassifier(AbstractClassifier
		// .makeCopy(baseClassifier));

		int k;

		if (useOnlyLabels) {
			int x;
			indicesToRemove = new int[nattrs - numLabels - 1];
			for (x = 0, k = 0; k < nattrs - numLabels; k++) {
				if (k == labelIndices[labeli]) {
					indicesToRemove[k] = trainDataset.numAttributes() - numLabels + labeli;
					continue;
				}
				indicesToRemove[x] = k;
				x++;
			}
		} else {
			if (useMirrorLabel) {
				indicesToRemove = new int[numLabels - 1];
			} else {
				indicesToRemove = new int[numLabels];
			}
			for (k = 0; k < labeli; k++) {
				indicesToRemove[k] = labelIndices[k];
			}

			int k2 = k + 1;
			if (!useMirrorLabel) {
				indicesToRemove[k] = trainDataset.numAttributes() - numLabels + labeli;
				k++;
			}

			for (; k2 < numLabels; k++, k2++) {
				indicesToRemove[k] = labelIndices[k2];
			}
		}

		Remove remove = new Remove();
		remove.setAttributeIndicesArray(indicesToRemove);
		remove.setInputFormat(trainDataset);
		remove.setInvertSelection(false);

		fc.setFilter(remove);
		return fc;
	}

	private void buildChainClassifier(int c, Instances newtrainData) throws Exception {
		for (int j = 0; j < numLabels; j++) {
			if (!useTrainPropag) {
				if (c > 0) {
					ensemble[c][j] = ensemble[0][j];
					continue;
				}
			}

			ensemble[c][j] = constructClassifier(j, newtrainData);

			newtrainData.setClassIndex(labelIndices[j]);

			ensemble[c][j].buildClassifier(newtrainData);
		}
	}

	private Instances generateData(Instances trainData) throws Exception {
		addsattr = new Add[numLabels];
		Instances newtrainData = trainData;
		for (int j = 0; j < numLabels; j++) {
			addsattr[j] = new Add();
			addsattr[j].setOptions(new String[] { "-T", "NUM" });

			addsattr[j].setAttributeIndex("last");
			addsattr[j].setAttributeName("labelAttr" + j);
			addsattr[j].setInputFormat(newtrainData);

			newtrainData = Filter.useFilter(newtrainData, addsattr[j]);
		}
		for (int i = 0; i < newtrainData.numInstances(); i++) {
			Instance inst = newtrainData.instance(i);
			for (int j = 0; j < numLabels; j++) {
				inst.setValue(j + trainData.numAttributes(), inst.value(labelIndices[j]));
			}
		}
		return newtrainData;
	}

	@Override
	protected void buildInternal(MultiLabelInstances train) throws Exception {
		buildInternal2(train);
	}

	private boolean hit(MultiLabelOutput mlo, Instance inst) {
		boolean[] bip = mlo.getBipartition();
		for (int i = 0; i < numLabels; i++) {
			if (bip[i]) {
				if (inst.value(labelIndices[i]) == 0) {
					return false;
				}
			} else {
				if (inst.value(labelIndices[i]) == 1) {
					return false;
				}
			}
		}
		return true;
	}

	private void buildInternal2(MultiLabelInstances train) throws Exception {
		numLabels = train.getNumLabels();
		trainData = train.getDataSet();
		int numinsts = trainData.numInstances();

		ensemble = new FilteredClassifier[chainSize][numLabels];

		newtrainData = generateData(trainData);

		// debug("Bulding model BR");
		baseml.build(train);

		indexs.clear();
		if (useTrainPropag) {
			debug("Propagating Prediction 0");
			for (int i = numinsts - 1; i >= 0; i--) {

				Instance inst = newtrainData.instance(i);

				MultiLabelOutput mlo = baseml.makePrediction(inst);

				if (useTrainPropag) {
					TransformInstance(inst, mlo);
				}

				if (hit(mlo, inst)) {
					indexs.add(i);
				}
			}
		}

		if (chainSize == 0) {
			return;
		}

		for (int c = 0; c < chainSize - 1; c++) {
			buildChainClassifier(c, newtrainData);
			if (useTrainPropag) {
				debug("Propagating Prediction " + (c + 1));

				for (int i = numinsts - 1; i >= 0; i--) {
					Instance inst = newtrainData.instance(i);
					MultiLabelOutput mlo;

					mlo = ChainMakePrediction(c, inst);
					if (useTrainPropag) {
						TransformInstance(inst, mlo);
					}

					if (hit(mlo, inst)) {
						indexs.add(i);
					}
				}
			}
		}

		realchainSize = chainSize;

		buildChainClassifier(chainSize - 1, newtrainData);
	}

	@Override
	protected MultiLabelOutput makePredictionInternal(Instance instance) throws Exception {
		maxevals = numLabels * realchainSize;
		Instance tempInstance = instance;
		int num_feats = instance.numAttributes() - numLabels;
		// System.out.println("makePredictionInternal IN");

		// FileWriter fw = new FileWriter(new File("/tmp/debugMRLM"), true);

		for (int j = 0; j < numLabels; j++) {
			addsattr[j].input(tempInstance);
			tempInstance = addsattr[j].output();
		}
		MultiLabelOutput[] mloensemble = new MultiLabelOutput[realchainSize + 1];
		mloensemble[0] = baseml.makePrediction(instance);

		mlbase_bipar = mloensemble[0].getBipartition();
		mlbase_conf = mloensemble[0].getConfidences();
		bestconfs = mlbase_conf.clone();

		num_attrs_submitted += num_feats * numLabels;

		for (int c = 0; c < realchainSize; c++) {
			mloensemble[c + 1] = ChainMakePrediction(c, tempInstance, mloensemble[c]);
			num_attrs_submitted += (num_feats + numLabels - 1) * numLabels;

			if (!useTrainPropag) {
				boolean[] bipart1 = mloensemble[c + 1].getBipartition();
				boolean[] bipart0 = mloensemble[c].getBipartition();
				boolean equals = true;

				for (int i = 0; i < bipart0.length; i++) {
					if (bipart0[i] != bipart1[i]) {
						equals = false;
						break;
					}
				}

				if (equals) {
					// System.out.println("stopping propagation at "+c);
					// if (c > max_c) {
					// max_c = c;
					// fw.write(sdf.format(new Date()) + ">" + "max stop="
					// + max_c + "\n");
					// }
					// fw.close();
					if (mlocombtype == 1) {
						return combineMLO(mloensemble, c + 1);
					}
					return mloensemble[c];
				}
			}
		}

		// if (max_c < realchainSize) {
		// max_c = realchainSize;
		// fw.write(sdf.format(new Date()) + ">" + "max stop=" + max_c + "\n");
		// }

		// fw.close();

		if (mlocombtype == 1) {
			return combineMLO(mloensemble, mloensemble.length);
		}
		return mloensemble[realchainSize];

	}

	private MultiLabelOutput combineMLO(MultiLabelOutput[] mloensemble, int nen) {
		final int nl = mloensemble[0].getBipartition().length;
		boolean[] Fbipart = new boolean[nl];
		double[] Fconf = new double[nl];
		for (int i = 0; i < Fconf.length; i++) {
			Fconf[i] = mloensemble[nen - 1].getConfidences()[i];
		}

		int[] n1 = new int[nl];
		int[] n0 = new int[nl];

		for (int i = 0; i < nen; i++) {
			boolean[] b = mloensemble[i].getBipartition();
			for (int j = 0; j < b.length; j++) {
				if (b[j]) {
					n1[j]++;
				} else {
					n0[j]++;
				}
			}
		}
		for (int i = 0; i < nl; i++) {
			Fbipart[i] = n1[i] >= n0[i];
		}
		return new MultiLabelOutput(Fbipart, Fconf);
	}

	private MultiLabelOutput combineMLO2(MultiLabelOutput[] mloensemble) {

		if (realchainSize == 0) {
			return mloensemble[0];
		}

		double[] c = mloensemble[0].getConfidences();
		boolean[] Fbipart = new boolean[mloensemble[0].getBipartition().length];
		double[] Fconf = new double[mloensemble[0].getConfidences().length];
		double maxprod = c[0];
		Fconf[0] = c[0];
		for (int i = 1; i < Fconf.length; i++) {
			Fconf[i] = c[i];
			maxprod *= c[i] >= 0.5 ? c[i] : 1 - c[i];
		}

		for (int i = 1; i < mloensemble.length; i++) {
			double[] conf = mloensemble[i].getConfidences();
			double prod = conf[0];

			for (int j = 1; j < conf.length; j++) {
				prod *= c[j] >= 0.5 ? c[j] : 1 - c[j];
			}
			if (prod > maxprod) {
				maxprod = prod;
				for (int j = 0; j < conf.length; j++) {
					Fconf[j] = c[j];
				}
			}
		}
		for (int i = 0; i < Fconf.length; i++) {
			Fbipart[i] = Fconf[i] > 0.5;
		}
		return new MultiLabelOutput(Fbipart, Fconf);
	}

	public void setTrainPropagation(boolean tp) {
		useTrainPropag = tp;
	}

	public void setUseOnlyLabels(boolean u) {
		useOnlyLabels = u;
	}

	public void setUseMirrorLabel(boolean u) {
		useMirrorLabel = u;
	}

	public void setUseConfiability(boolean u) {
		useConfiability = u;
	}

	public void setChainUpdate(boolean b) {
		chainUpdate = b;

	}

	public int getNum_attrs_submitted() {
		return num_attrs_submitted;
	}

	public int getChainSize() {
		return chainSize;
	}

	public void setUseSubIfConf(boolean u) {
		useSubIfConf = u;
	}

	public void setBackChain(boolean b) {
		backChain = b;
	}

	public boolean isBackChain() {
		return backChain;
	}

	public void setMLOCombination(int t) {
		mlocombtype = t;
	}

	public int getMLOCombination() {
		return mlocombtype;
	}
}
