/*
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package mulan.classifier.transformation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import mulan.classifier.InvalidDataException;
import mulan.classifier.MultiLabelLearnerGridSearch;
import mulan.classifier.MultiLabelOutput;
import mulan.data.InvalidDataFormatException;
import mulan.data.MultiLabelInstances;
import mulan.evaluation.Evaluation;
import mulan.evaluation.Evaluator;
import mulan.evaluation.MultipleEvaluation;
import mulan.evaluation.measure.ExampleBasedFMeasure;
import mulan.evaluation.measure.Measure;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.trees.J48;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.RemovePercentage;

/**
 * <p>
 * Implementation of the Ensemble of Pruned Sets (EPS) algorithm.</p>
 * <p>
 * For more information, see <em>Read, J.; Pfahringer, B.; Holmes, G. (2008)
 * Multi-label Classification using Ensembles of Pruned Sets. In: ICDM'08:
 * Eighth IEEE International Conference on Data Mining, 995-1000.</em></p>
 *
 * @author Emmanouela Stachtiari
 * @author Grigorios Tsoumakas
 * @version 2012.02.27
 */
public class EnsembleOfPrunedSetsDT extends TransformationBasedMultiLabelLearner implements MultiLabelLearnerGridSearch {

    /**
     * Parameter for the threshold of discretization of prediction output
     */
    protected double threshold;
    /**
     * Parameter for the number of models that constitute the ensemble
     */
    protected int numOfModels;
    /**
     * Percentage of data
     */
    protected double percentage;
    /**
     * The models in the ensemble
     */
    protected PrunedSets[] ensemble;
    /**
     * Random number generator
     */
    protected Random rand;
    private MLDT MultiLabelDecisionTemplate;
    private MLDT.PredictionType predictionType;
    private int aP;
    private int aB;
    private PrunedSets.Strategy aStrategy;
    private Classifier baselearner;

    /**
     * Creates a new instance with default values
     */
    public EnsembleOfPrunedSetsDT() {
        this(66, 10, 0.5, 2, PrunedSets.Strategy.A, 3, new J48(), MLDT.PredictionType.COMBINED);
    }

    /**
     * @param aNumOfModels the number of models in the ensemble
     * @param aStrategy pruned sets strategy
     * @param aPercentage percentage of data to sample
     * @param aP pruned sets parameter p
     * @param aB pruned sets parameter b
     * @param baselearner the base learner
     * @param aThreshold the threshold for producing bipartitions
     */
    public EnsembleOfPrunedSetsDT(double aPercentage, int aNumOfModels, double aThreshold, int aP, PrunedSets.Strategy aStrategy, int aB, Classifier baselearner, MLDT.PredictionType predictionType) {
        super(baselearner);
        numOfModels = aNumOfModels;
        threshold = aThreshold;
        percentage = aPercentage;
        ensemble = new PrunedSets[numOfModels];
        this.aP = aP;
        this.aB = aB;
        this.aStrategy = aStrategy;
        this.baselearner = baselearner;
        rand = new Random(1);
        this.predictionType = predictionType;
    }

    public MultiLabelLearnerGridSearch gridSearch(MultiLabelInstances train, MultiLabelInstances validation) throws InvalidDataFormatException {

        List<Measure> measures = new ArrayList<Measure>(1);
        measures.add(new ExampleBasedFMeasure());
        Evaluator eval = new Evaluator();
        double best = 0;
        Classifier[] baseLearners = {new NaiveBayes()};
        double[] aPercentages = {66, 75};
        PrunedSets.Strategy[] strategies = {PrunedSets.Strategy.A, PrunedSets.Strategy.B};
        int[] aps = {2};
        int[] bps = {3};
        MLDT.PredictionType[] mldtTypes = {MLDT.PredictionType.INDIVIDUAL, MLDT.PredictionType.COMBINED};

        for (Classifier baseLearner1 : baseLearners) {
            for (double aPercentage : aPercentages) {
                for (PrunedSets.Strategy str : strategies) {
                    for (int a : aps) {
                        for (int b : bps) {
                            for (MLDT.PredictionType mldtType : mldtTypes) {
                                try {
                                    EnsembleOfPrunedSetsDT ensembleOfPrunedSets = new EnsembleOfPrunedSetsDT(aPercentage, this.numOfModels, 0.5, a, str, b, baseLearner1, mldtType);
                                    ensembleOfPrunedSets.build(train);
                                    Evaluation evaluate = eval.evaluate(ensembleOfPrunedSets, validation, measures);
                                    double mean = evaluate.getValue(0);
                                    if (mean > best) {
                                        best = mean;
                                        this.percentage = aPercentage;
                                        this.aB = b;
                                        this.aP = a;
                                        this.baseClassifier = baseLearner1;
                                        this.aStrategy = str;
                                    }
                                } catch (Exception ex) {
                                    Logger.getLogger(EnsembleOfPrunedSetsDT.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }
                    }
                }
            }

        }
        return new EnsembleOfPrunedSetsDT(percentage, this.numOfModels, 0.5, aP, aStrategy, aB, baseClassifier, MLDT.PredictionType.COMBINED);
    }

    @Override
    protected void buildInternal(MultiLabelInstances trainingSet) throws Exception {

        for (int i = 0; i < numOfModels; i++) {
            try {
                ensemble[i] = new PrunedSets(AbstractClassifier.makeCopy(baselearner), aP, aStrategy, aB);
            } catch (Exception ex) {
                Logger.getLogger(EnsembleOfPrunedSetsDT.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Instances dataSet = new Instances(trainingSet.getDataSet());

        for (int i = 0; i < numOfModels; i++) {
            dataSet.randomize(rand);
            RemovePercentage rmvp = new RemovePercentage();
            rmvp.setInputFormat(dataSet);
            rmvp.setPercentage(percentage);
            rmvp.setInvertSelection(true);
            Instances trainDataSet = Filter.useFilter(dataSet, rmvp);
            MultiLabelInstances train = new MultiLabelInstances(trainDataSet, trainingSet.getLabelsMetaData());
            ensemble[i].build(train);
        }

        MultiLabelDecisionTemplate = new MLDT(ensemble, predictionType);
        MultiLabelDecisionTemplate.setBuilEach(false);
        MultiLabelDecisionTemplate.build(trainingSet);

    }

    @Override
    protected MultiLabelOutput makePredictionInternal(Instance instance)
            throws Exception, InvalidDataException {
        return MultiLabelDecisionTemplate.makePrediction(instance);
//        int[] sumVotes = new int[numLabels];
//
//        for (int i = 0; i < numOfModels; i++) {
//            MultiLabelOutput ensembleMLO = ensemble[i].makePrediction(instance);
//            boolean[] bip = ensembleMLO.getBipartition();
//
//            for (int j = 0; j < sumVotes.length; j++) {
//                sumVotes[j] += bip[j] == true ? 1 : 0;
//            }
//        }
//        double[] confidence = new double[numLabels];
//
//        for (int j = 0; j < sumVotes.length; j++) {
//            confidence[j] = (double) sumVotes[j] / (double) numOfModels;
//        }
//
//        MultiLabelOutput mlo = new MultiLabelOutput(confidence, threshold);
//        return mlo;
    }
}
