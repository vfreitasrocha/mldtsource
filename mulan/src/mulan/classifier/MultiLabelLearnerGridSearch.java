/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mulan.classifier;

import mulan.data.InvalidDataFormatException;
import mulan.data.MultiLabelInstances;

/**
 *
 * @author Victor
 */
public interface MultiLabelLearnerGridSearch extends MultiLabelLearner {
    public MultiLabelLearnerGridSearch gridSearch(MultiLabelInstances train, MultiLabelInstances validation) throws InvalidDataFormatException;
}
