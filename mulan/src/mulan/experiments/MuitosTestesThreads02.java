/*
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *    PatternRecognition07MLkNN.java
 *    Copyright (C) 2009-2012 Aristotle University of Thessaloniki, Greece
 */
package mulan.experiments;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import mulan.classifier.meta.RAkEL;
import mulan.classifier.meta.RAkELDT;
import mulan.classifier.transformation.EnsembleOfClassifierChains;
import mulan.classifier.transformation.EnsembleOfClassifierChainsDT;
import mulan.classifier.transformation.MLDT;
import mulan.classifier.transformation.RDBR;
import mulan.data.LabelSet;
import mulan.data.MultiLabelInstances;
import mulan.data.Statistics;
import mulan.evaluation.MultipleEvaluation;
import mulan.evaluation.Evaluator;
import mulan.evaluation.measure.*;
import weka.classifiers.bayes.NaiveBayes;
import weka.core.TechnicalInformation;
import weka.core.TechnicalInformation.Field;
import weka.core.TechnicalInformation.Type;
import weka.core.Utils;

/**
 * Experiments for my monograph
 *
 * @author Victor Freitas Rocha (victorfreitasr@gmail.com)
 * @version 2016.09.19
 */
public class MuitosTestesThreads02 implements Runnable {

    /**
     * Main class
     *
     * @param args command line arguments
     */
    static String[] args;
    static String basename;
    static int met = 0;

    public void run() {
        try {

            String path = Utils.getOption("path", args);
            String filestem = "data/" + basename;
            //String caminho = "/home/vrocha/Documents/Mulan/mulan-1.4.0/data/";
            //System.out.println(basename + " data set");
            //out.println(basename + " data set");
            MultiLabelInstances dataSet = new MultiLabelInstances(filestem + ".arff", filestem + ".xml");

            Evaluator eval = new Evaluator();
            MultipleEvaluation results;
            List<Measure> measures = new ArrayList<Measure>(6);
            measures.add(new ExampleBasedAccuracy());
            measures.add(new ExampleBasedPrecision());
            measures.add(new ExampleBasedRecall());
            measures.add(new HammingLoss());
            measures.add(new SubsetAccuracy());

            switch (met) {

                case 0:
                    Statistics stat = new Statistics();
                    stat.calculateStats(dataSet);
                    Set<LabelSet> labelSets = stat.labelSets();
                    System.out.println("Numero de classes: " + dataSet.getNumLabels());

                    double[] prob = stat.priors();
                    double freq = 0;

                    for (double d : prob) {
                        if (freq < d) {
                            freq = d;
                        }
                    }

                    System.out.println("Frequencia da classe dominante: " + freq);
                    break;
                case 1:
                    System.out.println("ECC-DT");
                    EnsembleOfClassifierChainsDT ensembleOfClassifierChainsDT = new EnsembleOfClassifierChainsDT(new NaiveBayes(), 2, true, true, MLDT.PredictionType.COMBINED);
                    results = eval.crossValidate(ensembleOfClassifierChainsDT, dataSet, measures, 10);
                    System.out.println(results);
                    break;
                case 2:
                    System.out.println("ECC");
                    EnsembleOfClassifierChains ensembleOfClassifierChains = new EnsembleOfClassifierChains(new NaiveBayes(), 2, true, true);
                    results = eval.crossValidate(ensembleOfClassifierChains, dataSet, measures, 10);
                    System.out.println(results);
                    break;
                case 3:
                    System.out.println("RAkEL");
                    RAkEL rak = new RAkEL();
                    results = eval.crossValidate(rak, dataSet, measures, 10);
                    System.out.println(results);
                case 4:
                    System.out.println("RAkEL-DT");
                    RAkELDT rakDT = new RAkELDT();
                    results = eval.crossValidate(rakDT, dataSet, measures, 10);
                    System.out.println(results);
                case 5:
                    System.out.println("RDBR");
                    RDBR rdbr = new RDBR(new NaiveBayes(), 2);
                    results = eval.crossValidate(rdbr, dataSet, measures, 10);
                    System.out.println(results);
                    break;
                default:
                    System.out.println("Invalid operation");
                    break;
            }

        } catch (InterruptedException ie) {
            System.out.println("Interruption " + ie);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws FileNotFoundException {
        List<String> bases = new ArrayList<String>(50);
        bases.add("emotions");
        //SMO com BR parece melhorar levemente
        for (String b : bases) {
            System.out.println("base:" + b);
            MuitosTestesThreads02.args = args;
            basename = b;

            try {
                for (int i = 3; i <= 4; i++) {
                    met = i;
//                    PrintStream arqout = new PrintStream(new FileOutputStream(basename + ".out", true));
//                    System.setOut(arqout);
//                    System.setErr(arqout);
                    Thread t = new Thread(new MuitosTestesThreads02(), basename + " Thread");
                    t.start();
                    t.join();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public TechnicalInformation getTechnicalInformation() {
        TechnicalInformation result;

        result = new TechnicalInformation(Type.ARTICLE);
        result.setValue(Field.AUTHOR, "Victor Freitas Rocha");
        result.setValue(Field.TITLE, "Novos testes 2016");
        result.setValue(Field.JOURNAL, "Pattern Recogn.");
        result.setValue(Field.VOLUME, "0");
        result.setValue(Field.NUMBER, "0");
        result.setValue(Field.YEAR, "2016");
        result.setValue(Field.ISSN, "0");
        result.setValue(Field.PAGES, "0");
        result.setValue(Field.PUBLISHER, "Ninfa");
        result.setValue(Field.ADDRESS, "Vitória, ES, Brasil");

        return result;
    }
}
