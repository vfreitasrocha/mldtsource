/*
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 *    PatternRecognition07MLkNN.java
 *    Copyright (C) 2009-2012 Aristotle University of Thessaloniki, Greece
 */
package mulan.experiments;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import mulan.classifier.transformation.EnsembleOfClassifierChains;
import mulan.data.LabelSet;
import mulan.data.MultiLabelInstances;
import mulan.data.Statistics;
import mulan.evaluation.Evaluator;
import mulan.evaluation.MultipleEvaluation;
import mulan.evaluation.measure.*;
import weka.classifiers.functions.SMO;
import weka.core.TechnicalInformation;
import weka.core.TechnicalInformation.Field;
import weka.core.TechnicalInformation.Type;
import weka.core.Utils;

/**
 * Class replicating an experiment from a published paper
 *
 * @author Eleftherios Spyromitros-Xioufis (espyromi@csd.auth.gr)
 * @version 2010.12.10
 */
public class MuitosTestes01 {

    /**
     * Main class
     *
     * @param args command line arguments
     */
    static String[] args;

    public static void main(String[] args) {
        MuitosTestes01.args = args;
        doAll("emotions");
       //doAll("motorpump");
       // doAll("emotions");
        //   doAll("basemulti");
        //   doAll("yeast");
        //   doAll("genbase");
     /*   doAll("Corel5k");
        doAll("delicious");
        doAll("eron");
        doAll("mediamill");
        doAll("medical");
        doAll("rcv1subset1");
        doAll("tmc2007");
        doAll("genbase");
         */
    }

    private static void doAll(String basename) {
        try {
            String path = Utils.getOption("path", args);
            String filestem = "data/" + basename;

            System.out.println(basename + " data set");
            //    out.println(basename + " data set");
            MultiLabelInstances dataSet = new MultiLabelInstances(path + filestem + ".arff", path + filestem + ".xml");
            //setstratfied
            Evaluator eval = new Evaluator();
            MultipleEvaluation results;
            List<Measure> measures = new ArrayList<Measure>(6);
            measures.add(new ExampleBasedAccuracy());
            measures.add(new SubsetAccuracy());
            measures.add(new ExampleBasedPrecision());
            measures.add(new ExampleBasedRecall());
            measures.add(new HammingLoss());
            measures.add(new RankingLoss());

            Statistics stat = new Statistics();
            stat.calculateStats(dataSet);
            //NUMERO DE LABELS: dataSet.getNumLabels();
            Set<LabelSet> labelSets = stat.labelSets();
            System.out.println("Numero de classes: " + labelSets.size());
            
            float freq = 0;
            for (LabelSet labelSet : labelSets) {
                if (stat.labelFrequency(labelSet) > freq) {
                    freq = stat.labelFrequency(labelSet);
                }
            }
            freq = freq / dataSet.getNumInstances();
            System.out.println("Frequencia da classe dominante: " + freq);

//            System.out.println("ECCDT");
//            EnsembleOfClassifierChainsDT ensembleOfClassifierChainsDT = new EnsembleOfClassifierChainsDT(new J48(), 10, true, true);
//            results = eval.crossValidate(ensembleOfClassifierChainsDT, dataSet, measures, 10);
//            System.out.println(results);


            System.out.println("ECC-SVM:");
            results = eval.crossValidate(new EnsembleOfClassifierChains(new SMO(), 10, true, true), dataSet, measures, 10);
            System.out.println(results.toCSV() + ";");
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public TechnicalInformation getTechnicalInformation() {
        TechnicalInformation result;

        result = new TechnicalInformation(Type.ARTICLE);
        result.setValue(Field.AUTHOR, "Victor Rocha");
        result.setValue(Field.TITLE, "Novos testes");
        result.setValue(Field.JOURNAL, "Pattern Recogn.");
        result.setValue(Field.VOLUME, "0");
        result.setValue(Field.NUMBER, "0");
        result.setValue(Field.YEAR, "2013");
        result.setValue(Field.ISSN, "0");
        result.setValue(Field.PAGES, "0");
        result.setValue(Field.PUBLISHER, "Ninfa");
        result.setValue(Field.ADDRESS, "Vitória, ES, Brasil");

        return result;
    }
}