/*
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

 /*
 *    PatternRecognition07MLkNN.java
 *    Copyright (C) 2009-2012 Aristotle University of Thessaloniki, Greece
 */
package mulan.experiments;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import mulan.classifier.lazy.BRkNN;
import mulan.classifier.lazy.MLkNN;
import mulan.classifier.neural.BPMLL;
import mulan.classifier.transformation.BinaryRelevance;
import mulan.classifier.transformation.ClassifierChain;
import mulan.classifier.transformation.EnsembleOfClassifierChains;
import mulan.data.LabelSet;
import mulan.data.MultiLabelInstances;
import mulan.data.Statistics;
import mulan.evaluation.Evaluator;
import mulan.evaluation.MultipleEvaluation;
import mulan.evaluation.measure.*;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.functions.SMO;
import weka.classifiers.lazy.IBk;
import weka.core.TechnicalInformation;
import weka.core.TechnicalInformation.Field;
import weka.core.TechnicalInformation.Type;
import weka.core.Utils;

/**
 * Class replicating an experiment from a published paper
 *
 * @author Eleftherios Spyromitros-Xioufis (espyromi@csd.auth.gr)
 * @version 2010.12.10
 */
public class MuitosTestesECC implements Runnable {

    /**
     * Main class
     *
     * @param args command line arguments
     */
    static String[] args;
    static String basename;
    static int met = 0;

    @Override
    public void run() {
        try {
            String path = Utils.getOption("path", args);
            String filestem = "data/";
            String caminho = filestem;
            
            //System.out.println(basename + " data set");
            //    out.println(basename + " data set");
            MultiLabelInstances dataSet = new MultiLabelInstances(caminho + basename + ".arff", caminho + basename + ".xml");

            Evaluator eval = new Evaluator();
            MultipleEvaluation results;
            List<Measure> measures = new ArrayList<Measure>(6);
            measures.add(new SubsetAccuracy());
            measures.add(new ExampleBasedAccuracy());
            measures.add(new ExampleBasedPrecision());
            measures.add(new ExampleBasedRecall());
            measures.add(new HammingLoss());
            measures.add(new RankingLoss());

            switch (met) {

                case 0:
                    Statistics stat = new Statistics();
                    stat.calculateStats(dataSet);
                    Set<LabelSet> labelSets = stat.labelSets();
                    System.out.println("Numero de classes: " + dataSet.getNumLabels());

                    double[] prob = stat.priors();
                    double freq = 0;

                    for (double d : prob) {
                        if (freq < d) {
                            freq = d;
                        }
                    }

                    System.out.println("Frequencia da classe dominante: " + freq);
                    break;

                case 1:
                    //System.out.println("MLkNN:");
                    double smooth = 1.0;
                    MLkNN mlknn = new MLkNN(12, smooth);
                    results = eval.crossValidate(mlknn, dataSet, measures, 10);
                    System.out.println(results.toCSV());
                    break;

                case 2:
                    //System.out.println("ML-ANN:");
                    results = eval.crossValidate(new BPMLL(), dataSet, measures, 10);
                    System.out.println(results.toCSV());
                    break;

                case 3:
                    //System.out.println("BR-kNN:");
                    results = eval.crossValidate(new BRkNN(12), dataSet, measures, 10);
                    System.out.println(results.toCSV());
                    break;

                case 4:
                    //System.out.println("BR-ANN:");
                    MultilayerPerceptron mp = new MultilayerPerceptron();
                    mp.setTrainingTime(1);

                    results = eval.crossValidate(new BinaryRelevance(mp), dataSet, measures, 10);
                    System.out.println(results.toCSV());
                    break;

                case 5:
                    //System.out.println("BR-SVM:");
                    results = eval.crossValidate(new BinaryRelevance(new SMO()), dataSet, measures, 10);
                    System.out.println(results.toCSV());
                    break;

                case 6:
                    //System.out.println("CC-KNN:");
                    results = eval.crossValidate(new BinaryRelevance(new IBk(12)), dataSet, measures, 10);
                    System.out.println(results.toCSV());
                    break;

                case 7:
                    //System.out.println("CC-LR:");
                    results = eval.crossValidate(new BinaryRelevance(new Logistic()), dataSet, measures, 10);
                    System.out.println(results.toCSV());
                    break;

                case 8:
                    //System.out.println("CC-ANN:");
                    MultilayerPerceptron mp2 = new MultilayerPerceptron();
                    mp2.setTrainingTime(1);
                    results = eval.crossValidate(new ClassifierChain(mp2), dataSet, measures, 10);
                    System.out.println(results.toCSV());
                    break;

                case 9:
                    //System.out.println("CC-SVM:");
                    results = eval.crossValidate(new ClassifierChain(new SMO()), dataSet, measures, 10);
                    System.out.println(results.toCSV());
                    break;

                case 10:
                    //System.out.println("ECC-ANN:");
                    MultilayerPerceptron mp3 = new MultilayerPerceptron();
                    mp3.setTrainingTime(1);
                    results = eval.crossValidate(new EnsembleOfClassifierChains(mp3, 10, true, true), dataSet, measures, 10);
                    System.out.println(results.toCSV());
                    break;

                case 11:
                    //System.out.println("ECC-SVM:");
                    for (int m = 0; m <= 10; m++) {
                        System.out.println("Number of models: " + m);
                        results = eval.crossValidate(new EnsembleOfClassifierChains(new SMO(), m, false, true), dataSet, measures, 10);
                        System.out.println(results.toCSV());
                    }
                    break;

                default:
                    System.out.println("Invalid operation");
                    break;
            }

        } catch (InterruptedException ie) {
            System.out.println("Interruption " + ie);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws FileNotFoundException {
        List<String> bases = new ArrayList<String>(50);

        bases.add("birds");
        bases.add("emotions");
        
        /*
        bases.add("yeast");
        bases.add("genbase");
        bases.add("CAL500");
        bases.add("yeast");
        bases.add("genbase");
        bases.add("Corel5k");
        bases.add("delicious");
        bases.add("eron");
        bases.add("mediamill");
        bases.add("medical");
        bases.add("rcv1subset1");
        bases.add("tmc2007");
        bases.add("Arts1");   
        bases.add("Business1");
        bases.add("Computers1");
        bases.add("Education1");
        bases.add("Entertainment1");
        bases.add("Health1");
        bases.add("Recreation1");
        bases.add("Reference1");
        bases.add("Science1");
        bases.add("Social1");
        bases.add("Society1");
        bases.add("Corel16k001");
        bases.add("Corel16k002");
        bases.add("Corel16k003");
        bases.add("Corel16k004");
        bases.add("Corel16k005");
        bases.add("Corel16k006");
        bases.add("Corel16k007");
        bases.add("Corel16k008");
        bases.add("Corel16k009");
        bases.add("Corel16k010");
        bases.add("bookmarks");
        bases.add("bibtex");
        bases.add("rcv1subset1");
        //bases.add("Corel5k");
         */

        for (String b : bases) {
            //MuitosTestesThreads01.args = args;
            basename = b;
            try {
                for (int i = 0; i < 12; i++) {
                    met = i;
//                    FileOutputStream fileOutputStream = new FileOutputStream(basename + ".out", true);
//                    PrintStream arqout = new PrintStream(fileOutputStream);
//                    System.setOut(arqout);
//                    System.setErr(arqout);
                    Thread t = new Thread(new MuitosTestesECC(), basename + " Thread");
                    t.start();
                    t.join();
                }
//            } catch (IOException ex) {
//                System.out.println("Nao deu pra ler da base");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public TechnicalInformation getTechnicalInformation() {
        TechnicalInformation result;

        result = new TechnicalInformation(Type.ARTICLE);
        result.setValue(Field.AUTHOR, "Victor Rocha");
        result.setValue(Field.TITLE, "Novos testes");
        result.setValue(Field.JOURNAL, "Pattern Recogn.");
        result.setValue(Field.VOLUME, "0");
        result.setValue(Field.NUMBER, "0");
        result.setValue(Field.YEAR, "2013");
        result.setValue(Field.ISSN, "0");
        result.setValue(Field.PAGES, "0");
        result.setValue(Field.PUBLISHER, "Ninfa");
        result.setValue(Field.ADDRESS, "Vitória, ES, Brasil");

        return result;
    }
}
