/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mulan.experiments;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import mulan.classifier.meta.EnsembleOfSubsetLearners;
import mulan.classifier.meta.EnsembleOfSubsetLearnersDT;
import mulan.classifier.meta.RAkEL;
import mulan.classifier.meta.RAkELDT;
import mulan.classifier.transformation.BinaryRelevance;
import mulan.classifier.transformation.EnsembleOfClassifierChains;
import mulan.classifier.transformation.EnsembleOfClassifierChainsDT;
import mulan.classifier.transformation.EnsembleOfPrunedSets;
import mulan.classifier.transformation.EnsembleOfPrunedSetsDT;
import mulan.classifier.transformation.MLDT;
import mulan.classifier.transformation.PrunedSets;
import mulan.data.ConditionalDependenceIdentifier;
import mulan.data.MultiLabelInstances;
import mulan.evaluation.MultipleEvaluation;
import mulan.evaluation.ValidationEvaluator;
import mulan.evaluation.measure.ExampleBasedAccuracy;
import mulan.evaluation.measure.ExampleBasedFMeasure;
import mulan.evaluation.measure.ExampleBasedPrecision;
import mulan.evaluation.measure.ExampleBasedRecall;
import mulan.evaluation.measure.HammingLoss;
import mulan.evaluation.measure.Measure;
import mulan.evaluation.measure.SubsetAccuracy;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.trees.J48;
import weka.core.TechnicalInformation;
import weka.core.Utils;

/**
 *
 * @author Victor
 */
public class MLDTGridSearch2018 implements Runnable {

    /**
     * Main class
     *
     * @param args command line arguments
     */
    static String[] args;
    static String basename;
    static int met = 0;
    /*Ensemble size value*/
    static int enSize = 2;
    /*Current position in the enSize array*/
    static int es = 0;
    static int nFolds = 10;//10
    static int cFold = 0;//current fold to evaluate
    private static PrintStream arqout;

    public void run() {
        try {
            String path = Utils.getOption("path", args);
            String filestem = "data/" + basename;
            //String caminho = "/home/vrocha/Documents/Mulan/mulan-1.4.0/data/";
            //System.out.println(basename + " data set");
            //out.println(basename + " data set");
            MultiLabelInstances dataSet = new MultiLabelInstances(filestem + ".arff", filestem + ".xml");
            
            ValidationEvaluator eval = new ValidationEvaluator();
            MultipleEvaluation results;
            List<Measure> measures = new ArrayList<Measure>(6);
            measures.add(new ExampleBasedAccuracy());
            measures.add(new ExampleBasedPrecision());
            measures.add(new ExampleBasedRecall());
            measures.add(new HammingLoss());
            measures.add(new SubsetAccuracy());
            measures.add(new ExampleBasedFMeasure());

            String metricName = new ExampleBasedFMeasure().getName();

            switch (met) {
                case 0:
                    System.out.println(measures.toString());
                case 1:
                    System.out.print("RAkEL;" + enSize + ";");
                    RAkEL rak = new RAkEL(new BinaryRelevance(new J48()));
                    rak.setNumModels(enSize);
                    results = eval.crossValidate(rak, dataSet, measures, nFolds, cFold);
                    System.out.println(results.toCSVMean());
                    break;
                case 2:
                    System.out.print("RAkEL-DTa;" + enSize + ";");
                    RAkELDT rakDT = new RAkELDT(new BinaryRelevance(new J48()), MLDT.PredictionType.COMBINED);
                    rakDT.setNumModels(enSize);
                    results = eval.crossValidate(rakDT, dataSet, measures, nFolds, cFold);
                    System.out.println(results.toCSVMean());
                    break;
                case 3:
                    System.out.print("ECC;" + enSize + ";");
                    EnsembleOfClassifierChains ensembleOfClassifierChains = new EnsembleOfClassifierChains(new NaiveBayes(), enSize, true, true);
                    results = eval.crossValidate(ensembleOfClassifierChains, dataSet, measures, nFolds, cFold);
                    System.out.println(results.toCSVMean());
                    break;
                case 4:
                    System.out.print("ECC-DT;" + enSize + ";");
                    EnsembleOfClassifierChainsDT ensembleOfClassifierChainsDT = new EnsembleOfClassifierChainsDT(new NaiveBayes(), enSize, true, true, MLDT.PredictionType.COMBINED);
                    results = eval.crossValidate(ensembleOfClassifierChainsDT, dataSet, measures, nFolds, cFold);
                    System.out.println(results.toCSVMean());
                    break;
                case 5:
                    System.out.print("EPS;" + enSize + ";");
                    EnsembleOfPrunedSets ensembleOfPrunedSets = new EnsembleOfPrunedSets(66, enSize, 0.5, 2, PrunedSets.Strategy.A, 3, new J48());
                    results = eval.crossValidate(ensembleOfPrunedSets, dataSet, measures, nFolds, cFold);
                    System.out.println(results.toCSVMean());
                    break;
                case 6:
                    System.out.print("EPS-DT;" + enSize + ";");
                    EnsembleOfPrunedSetsDT ensembleOfPrunedSetsDT = new EnsembleOfPrunedSetsDT(66, enSize, 0.5, 2, PrunedSets.Strategy.A, 3, new J48(), MLDT.PredictionType.COMBINED);
                    results = eval.crossValidate(ensembleOfPrunedSetsDT, dataSet, measures, nFolds, cFold);
                    System.out.println(results.toCSVMean());
                    break;
                case 7:
                    System.out.print("ESL;" + enSize + ";");
                    EnsembleOfSubsetLearners esl = new EnsembleOfSubsetLearners(new BinaryRelevance(new NaiveBayes()), new NaiveBayes(), new ConditionalDependenceIdentifier(new J48()), enSize);
                    results = eval.crossValidate(esl, dataSet, measures, nFolds, cFold);
                    System.out.println(results.toCSVMean());
                    break;
                case 8:
                    System.out.print("ESL-DT;" + enSize + ";");
                    EnsembleOfSubsetLearnersDT esldt1 = new EnsembleOfSubsetLearnersDT(new BinaryRelevance(new NaiveBayes()), new NaiveBayes(), new ConditionalDependenceIdentifier(new J48()), enSize, MLDT.PredictionType.COMBINED);
                    results = eval.crossValidate(esldt1, dataSet, measures, nFolds, cFold);
                    System.out.println(results.toCSVMean());
                    break;
                default:
                    System.out.println("Invalid operation");
                    break;
            }

        } catch (InterruptedException ie) {
            System.out.println("Interruption " + ie);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void batchmain(String[] args) throws FileNotFoundException {
        int[] ensizes = {2, 4, 8, 16, 32, 64, 128, 256};

        if (args.length == 0) {
            System.out.println("Parameters: (database, method [1-8], ensemble size)");
            return;
        } else {
            basename = args[0];
            met = Integer.parseInt(args[1]);
            enSize = Integer.parseInt(args[2]);
        }

        List<String> bases = new ArrayList<>();
        /*PROBLEMATIC DATABASES*/
//        bases.add("flags");
//        bases.add("emotions");
//        bases.add("birds");
//        bases.add("genbase");
//        bases.add("scene");
//        bases.add("genbase");//rapido
//        bases.add("enron");//demora
//        bases.add("CAL500");//demora
//        bases.add("medical");//demora
//        bases.add("bibtex");//infinito
//        bases.add("delicious");//problema
//        bases.add("rcv1subset1");//infinito
//        bases.add("Corel5k");//OOM
//        bases.add("tmc2007");
//        bases.add("mediamill");
//        bases.add("bookmarks");
//        bases.add("genbase");
//        bases.add("medical");
        //SMO com BR parece melhorar levemente
        for (String b : args) {
            TestesMultilabelDecisionTemplateES.args = args;
            basename = b;
            System.out.println(b);
            try {
                for (int i = 1; i <= 8; i++) {
                    for (es = 0; es < ensizes.length; es++) {
                        met = i;
                        enSize = ensizes[es];
                        arqout = new PrintStream(new FileOutputStream(basename + ".out", true));
                        Thread t = new Thread(new MLDTGridSearch2018(), basename + " Thread");
                        t.start();
                        t.join();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public static void main(String[] args) throws FileNotFoundException {
        //int[] ensizes = {2, 4, 8, 16, 32, 64, 128, 256};

        if (args.length == 0) {
            System.out.println("Parameters: (database, ensemble size, method [1-8], nFolds, cFold)");
            return;
        } else {
            basename = args[0];
            enSize = Integer.parseInt(args[1]);
            met = Integer.parseInt(args[2]);
            nFolds = Integer.parseInt(args[3]);
            cFold = Integer.parseInt(args[4]);
        }

        System.out.println(basename);
        try {
            arqout = new PrintStream(new FileOutputStream(basename + ".out", true));
            Thread t = new Thread(new MLDTGridSearch2018(), basename + " Thread");
            t.start();
            t.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public TechnicalInformation getTechnicalInformation() {
        TechnicalInformation result;

        result = new TechnicalInformation(TechnicalInformation.Type.ARTICLE);
        result.setValue(TechnicalInformation.Field.AUTHOR, "Victor Freitas Rocha");
        result.setValue(TechnicalInformation.Field.TITLE, "PG 2016/2");
        result.setValue(TechnicalInformation.Field.JOURNAL, "Pattern Recogn.");
        result.setValue(TechnicalInformation.Field.VOLUME, "0");
        result.setValue(TechnicalInformation.Field.NUMBER, "0");
        result.setValue(TechnicalInformation.Field.YEAR, "2016");
        result.setValue(TechnicalInformation.Field.ISSN, "0");
        result.setValue(TechnicalInformation.Field.PAGES, "0");
        result.setValue(TechnicalInformation.Field.PUBLISHER, "Ninfa");
        result.setValue(TechnicalInformation.Field.ADDRESS, "Vitória, ES, Brasil");

        return result;
    }

}
