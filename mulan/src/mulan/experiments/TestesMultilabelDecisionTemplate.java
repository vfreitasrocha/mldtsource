package mulan.experiments;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import mulan.classifier.meta.EnsembleOfSubsetLearners;
import mulan.classifier.meta.EnsembleOfSubsetLearnersDT;
import mulan.classifier.meta.RAkEL;
import mulan.classifier.meta.RAkELDT;
import mulan.classifier.meta.RAkELdDT;
import mulan.classifier.transformation.BinaryRelevance;
import mulan.classifier.transformation.EnsembleOfClassifierChains;
import mulan.classifier.transformation.EnsembleOfClassifierChainsDT;
import mulan.classifier.transformation.EnsembleOfPrunedSets;
import mulan.classifier.transformation.EnsembleOfPrunedSetsDT;
import mulan.classifier.transformation.MLDT;
import mulan.classifier.transformation.MRLM;
import mulan.classifier.transformation.MRLMDT;
import mulan.classifier.transformation.PrunedSets;
import mulan.data.ConditionalDependenceIdentifier;
import mulan.data.LabelSet;
import mulan.data.MultiLabelInstances;
import mulan.data.Statistics;
import mulan.evaluation.MultipleEvaluation;
import mulan.evaluation.Evaluator;
import mulan.evaluation.measure.*;
import weka.classifiers.trees.J48;
import weka.core.TechnicalInformation;
import weka.core.TechnicalInformation.Field;
import weka.core.TechnicalInformation.Type;
import weka.core.Utils;

/**
 * Experiments for my graduation project
 *
 * @author Victor Freitas Rocha (victorfreitasr@gmail.com)
 * @version 2016.09.19
 */
public class TestesMultilabelDecisionTemplate implements Runnable {

    /**
     * Main class
     *
     * @param args command line arguments
     */
    static String[] args;
    static String basename;
    static int met = 0;
    static int nFolds = 2;
    static int enSize = 10;

    public void run() {
        try {

            String path = Utils.getOption("path", args);
            String filestem = "data/" + basename;
            //String caminho = "/home/vrocha/Documents/Mulan/mulan-1.4.0/data/";
            //System.out.println(basename + " data set");
            //out.println(basename + " data set");
            MultiLabelInstances dataSet = new MultiLabelInstances(filestem + ".arff", filestem + ".xml");

            Evaluator eval = new Evaluator();
            MultipleEvaluation results;
            List<Measure> measures = new ArrayList<Measure>(6);
            measures.add(new ExampleBasedAccuracy());
            measures.add(new ExampleBasedPrecision());
            measures.add(new ExampleBasedRecall());
            measures.add(new ExampleBasedFMeasure());
            measures.add(new HammingLoss());
            measures.add(new SubsetAccuracy());

            switch (met) {

                case 9:
                    Statistics stat = new Statistics();
                    stat.calculateStats(dataSet);
                    Set<LabelSet> labelSets = stat.labelSets();
                    System.out.println("Numero de classes: " + dataSet.getNumLabels());
                    System.out.println("Cardinalidade: " + dataSet.getCardinality());
                    System.out.println("Numero de instâncias: " + dataSet.getNumInstances());
                    double[] prob = stat.priors();
                    double freq = 0;

                    for (double d : prob) {
                        if (freq < d) {
                            freq = d;
                        }
                    }

                    System.out.println("Frequencia da classe dominante: " + freq);
                    break;

//                case 1:
//                    System.out.println("RAkELd-DT");
//                    RAkELdDT rakdDT = new RAkELdDT();
//                    results = eval.crossValidate(rakdDT, dataSet, measures, 10);
//                    System.out.println(results);
//                    break;
//                    
//                case 2:
//                    System.out.println("RAkELd");
//                    RAkELd rakd = new RAkELd();
//                    results = eval.crossValidate(rakd, dataSet, measures, 10);
//                    System.out.println(results);
//                    break;
//                     
                case 10:
                    System.out.println("RDBR-DT");
                    MRLMDT rdbr2 = new MRLMDT(new J48(), enSize);
                    results = eval.crossValidate(rdbr2, dataSet, measures, nFolds);
                    System.out.println(results);
                    break;
                case 11:
                    System.out.println("RDBR");
                    MRLM rdbr = new MRLM(new J48(), enSize);
                    results = eval.crossValidate(rdbr, dataSet, measures, nFolds);
                    System.out.println(results);
                    break;
                case 12:
                    System.out.println("RAkEL-DTa");
                    RAkELdDT rakDT = new RAkELdDT(new BinaryRelevance(new J48()), MLDT.PredictionType.COMBINED);
                    results = eval.crossValidate(rakDT, dataSet, measures, nFolds);
                    System.out.println(results);
                    break;
                case 13:
                    System.out.println("RAkEL-DTb");
                    RAkELDT rakDT2 = new RAkELDT(new BinaryRelevance(new J48()), MLDT.PredictionType.INDIVIDUAL);
                    results = eval.crossValidate(rakDT2, dataSet, measures, nFolds);
                    System.out.println(results);
                    break;
                case 14:
                    System.out.println("RAkEL");
                    RAkEL rak = new RAkEL();
                    results = eval.crossValidate(rak, dataSet, measures, nFolds);
                    System.out.println(results);
                    break;
                case 0:
                    System.out.println("ECC");
                    EnsembleOfClassifierChains ensembleOfClassifierChains = new EnsembleOfClassifierChains(new J48(), enSize, true, true);
                    results = eval.crossValidate(ensembleOfClassifierChains, dataSet, measures, nFolds);
                    System.out.println(results);
                    break;
                case 1:
                    System.out.println("ECC-DTa");
                    EnsembleOfClassifierChainsDT ensembleOfClassifierChainsDT = new EnsembleOfClassifierChainsDT(new J48(), enSize, true, true, MLDT.PredictionType.COMBINED);
                    results = eval.crossValidate(ensembleOfClassifierChainsDT, dataSet, measures, nFolds);
                    System.out.println(results);
                    break;
                case 2:
                    System.out.println("ECC-DTb");
                    EnsembleOfClassifierChainsDT ensembleOfClassifierChainsDT2 = new EnsembleOfClassifierChainsDT(new J48(), enSize, true, true, MLDT.PredictionType.INDIVIDUAL);
                    results = eval.crossValidate(ensembleOfClassifierChainsDT2, dataSet, measures, nFolds);
                    System.out.println(results);
                    break;
                case 3:
                    System.out.println("EPS");
                    EnsembleOfPrunedSets ensembleOfPrunedSets = new EnsembleOfPrunedSets(66, enSize, 0.5, 2, PrunedSets.Strategy.A, 3, new J48());
                    results = eval.crossValidate(ensembleOfPrunedSets, dataSet, measures, nFolds);
                    System.out.println(results);
                    break;
                case 4:
                    System.out.println("EPS-DTa");
                    EnsembleOfPrunedSetsDT ensembleOfPrunedSetsDT = new EnsembleOfPrunedSetsDT(66, enSize, 0.5, 2, PrunedSets.Strategy.A, 3, new J48(), MLDT.PredictionType.COMBINED);
                    results = eval.crossValidate(ensembleOfPrunedSetsDT, dataSet, measures, nFolds);
                    System.out.println(results);
                    break;
                case 5:
                    System.out.println("EPS-DTb");
                    EnsembleOfPrunedSetsDT ensembleOfPrunedSetsDT2 = new EnsembleOfPrunedSetsDT(66, enSize, 0.5, 2, PrunedSets.Strategy.A, 3, new J48(), MLDT.PredictionType.INDIVIDUAL);
                    results = eval.crossValidate(ensembleOfPrunedSetsDT2, dataSet, measures, nFolds);
                    System.out.println(results);
                    break;
                case 6:
                    System.out.println("ESL");
                    EnsembleOfSubsetLearners esl = new EnsembleOfSubsetLearners(new BinaryRelevance(new J48()), new J48(), new ConditionalDependenceIdentifier(new J48()), enSize);
                    results = eval.crossValidate(esl, dataSet, measures, nFolds);
                    System.out.println(results);
                    break;
                case 7:
                    System.out.println("ESL DTa");
                    EnsembleOfSubsetLearnersDT esldt1 = new EnsembleOfSubsetLearnersDT(new BinaryRelevance(new J48()), new J48(), new ConditionalDependenceIdentifier(new J48()), enSize, MLDT.PredictionType.COMBINED);
                    results = eval.crossValidate(esldt1, dataSet, measures, nFolds);
                    System.out.println(results);
                    break;
                case 8:
                    System.out.println("ESL DTb");
                    EnsembleOfSubsetLearnersDT esldt2 = new EnsembleOfSubsetLearnersDT(new BinaryRelevance(new J48()), new J48(), new ConditionalDependenceIdentifier(new J48()), enSize, MLDT.PredictionType.INDIVIDUAL);
                    results = eval.crossValidate(esldt2, dataSet, measures, nFolds);
                    System.out.println(results);
                    break;
                default:
                    System.out.println("Invalid operation");
                    break;
            }

        } catch (InterruptedException ie) {
            System.out.println("Interruption " + ie);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws FileNotFoundException {

        TestesMultilabelDecisionTemplate.args = args;
        basename = args[0];
        try {
            int i = Integer.parseInt(args[1]);
            /**
             * Output folder: C:\Users\Victor\Documents\mldt
             */
            met = i;
            File file = new File(basename + i + ".out");
            System.out.println(Arrays.toString(args) + ">>>" + file.getPath());
            FileOutputStream fileOutputStream = new FileOutputStream(file, true);
            PrintStream arqout = new PrintStream(fileOutputStream);
            System.setOut(arqout);
            System.setErr(arqout);

            Thread t = new Thread(new TestesMultilabelDecisionTemplate(), basename + i + " Thread");
            t.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//  TODO: uma thread para cada método (basename+i). mais bases de dados. tirar arraylist bases
//  FIXME:Rakel ensemble size is probably wrong
    public static void mainTESTE(String[] args) throws FileNotFoundException {

//        List<String> bases = new ArrayList<>();
//        bases.add("emotions");
//        /*PROBLEMATIC DATABASES*/
//        bases.add("reuters");
//        bases.add("flags");
//        bases.add("emotions");//rapido
//        bases.add("birds");
//        bases.add("image");
//        bases.add("yeast");
//        bases.add("scene");
//        bases.add("genbase");//rapido
//        bases.add("enron");//demora
//        bases.add("CAL500");//demora
//        bases.add("medical");//demora
//        bases.add("bibtex");//infinito
//        bases.add("delicious");//problema
//        bases.add("rcv1subset1");//infinito
//        bases.add("Corel5k");//OOM
//        bases.add("tmc2007");
//        bases.add("mediamill");
//        bases.add("bookmarks");
//        bases.add("genbase");
//        bases.add("medical");
        //SMO com BR parece melhorar levemente
        for (String b : args) {
//        for (String b : bases) {
            TestesMultilabelDecisionTemplate.args = args;
            basename = b;
            System.out.println(basename);
            try {
                for (int i = 10; i <= 12; i++) {
                    if (i == 0) {
                        continue;
                    }
                    /**
                     * Output folder: C:\Users\Victor\Documents\mldt
                     */
                    met = i;
                    FileOutputStream fileOutputStream = new FileOutputStream(basename + i + ".out", true);
                    PrintStream arqout = new PrintStream(fileOutputStream);
                    System.setOut(arqout);
                    System.setErr(arqout);

                    Thread t = new Thread(new TestesMultilabelDecisionTemplate(), basename + i + " Thread");
                    t.start();
                    t.join();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public TechnicalInformation getTechnicalInformation() {
        TechnicalInformation result;

        result = new TechnicalInformation(Type.ARTICLE);
        result.setValue(Field.AUTHOR, "Victor Freitas Rocha");
        result.setValue(Field.TITLE, "PG 2016/2");
        result.setValue(Field.JOURNAL, "Pattern Recogn.");
        result.setValue(Field.VOLUME, "0");
        result.setValue(Field.NUMBER, "0");
        result.setValue(Field.YEAR, "2016");
        result.setValue(Field.ISSN, "0");
        result.setValue(Field.PAGES, "0");
        result.setValue(Field.PUBLISHER, "Ninfa");
        result.setValue(Field.ADDRESS, "Vitória, ES, Brasil");

        return result;
    }
}
