package mulan.experiments;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import mulan.classifier.meta.EnsembleOfSubsetLearners;
import mulan.classifier.meta.EnsembleOfSubsetLearnersDT;
import mulan.classifier.meta.RAkEL;
import mulan.classifier.meta.RAkELDT;
import mulan.classifier.meta.RAkELdDT;
import mulan.classifier.transformation.BinaryRelevance;
import mulan.classifier.transformation.EnsembleOfClassifierChains;
import mulan.classifier.transformation.EnsembleOfClassifierChainsDT;
import mulan.classifier.transformation.EnsembleOfPrunedSets;
import mulan.classifier.transformation.EnsembleOfPrunedSetsDT;
import mulan.classifier.transformation.MLDT;
import mulan.classifier.transformation.MRLM;
import mulan.classifier.transformation.MRLMDT;
import mulan.classifier.transformation.PrunedSets;
import mulan.data.ConditionalDependenceIdentifier;
import mulan.data.MultiLabelInstances;
import mulan.evaluation.MultipleEvaluation;
import mulan.evaluation.Evaluator;
import mulan.evaluation.measure.*;
import static mulan.experiments.TestesMultilabelDecisionTemplate.enSize;
import weka.classifiers.trees.J48;
import weka.core.TechnicalInformation;
import weka.core.TechnicalInformation.Field;
import weka.core.TechnicalInformation.Type;
import weka.core.Utils;

/**
 * Experiments for my graduation project
 *
 * @author Victor Freitas Rocha (victorfreitasr@gmail.com)
 * @version 2016.09.19
 */
public class TestesMultilabelDecisionTemplateES implements Runnable {

    /**
     * Main class
     *
     * @param args command line arguments
     */
    static String[] args;
    static String basename;
    static int met = 0;
    /*Ensemble size value*/
    static int enSize = 2;
    /*Current position in the enSize array*/
    static int es = 0;
    static int nFolds = 2;
    private static PrintStream arqout;

    /*Stores the metric reading that is the basis of the percentual comparizon.*/
    private static double[] metricBasisValues;

    /*Stores the metric reading that is the delta (the difference) of the percentual comparizon.*/
    private static double[] metricDeltaValues;

    public void run() {
        try {
            String path = Utils.getOption("path", args);
            String filestem = "data/" + basename;
            //String caminho = "/home/vrocha/Documents/Mulan/mulan-1.4.0/data/";
            //System.out.println(basename + " data set");
            //out.println(basename + " data set");
            MultiLabelInstances dataSet = new MultiLabelInstances(filestem + ".arff", filestem + ".xml");

            Evaluator eval = new Evaluator();
            MultipleEvaluation results;
            List<Measure> measures = new ArrayList<Measure>(6);
//            measures.add(new ExampleBasedAccuracy());
//            measures.add(new ExampleBasedPrecision());
//            measures.add(new ExampleBasedRecall());
//            measures.add(new HammingLoss());
//            measures.add(new SubsetAccuracy());
            measures.add(new ExampleBasedFMeasure());

            String metricName = new ExampleBasedFMeasure().getName();

            switch (met) {

                case -2:
                    System.out.println("RDBR-DT");
                    MRLMDT rdbr2 = new MRLMDT(new J48(), enSize);
                    results = eval.crossValidate(rdbr2, dataSet, measures, nFolds);
                    System.out.println(results);
                    break;
                case -1:
                    System.out.println("RDBR");
                    MRLM rdbr = new MRLM(new J48(), enSize);
                    results = eval.crossValidate(rdbr, dataSet, measures, nFolds);
                    System.out.println(results);
                    metricDeltaValues[es] = (results.getMean(metricName) - metricBasisValues[es]) / (metricBasisValues[es]);
                    break;
                case 2:
                    System.out.println("RAkEL-DTa");
                    RAkELDT rakDT = new RAkELDT(new BinaryRelevance(new J48()), MLDT.PredictionType.COMBINED);
                    rakDT.setNumModels(enSize);
                    results = eval.crossValidate(rakDT, dataSet, measures, nFolds);
                    System.out.println(results);
                    metricDeltaValues[es] = (results.getMean(metricName) - metricBasisValues[es]) / (metricBasisValues[es]);
                    break;
                case 3:
                    System.out.println("RAkEL-DTb");
                    RAkELDT rakDT2 = new RAkELDT(new BinaryRelevance(new J48()), MLDT.PredictionType.INDIVIDUAL);
                    results = eval.crossValidate(rakDT2, dataSet, measures, nFolds);
                    System.out.println(results);
                    metricDeltaValues[es] = (results.getMean(metricName) - metricBasisValues[es]) / (metricBasisValues[es]);
                    break;
                case 1:
                    System.out.println("RAkEL");
                    RAkEL rak = new RAkEL(new BinaryRelevance(new J48()));
                    rak.setNumModels(enSize);
                    results = eval.crossValidate(rak, dataSet, measures, nFolds);
                    System.out.println(results);
                    metricBasisValues[es] = results.getMean(metricName);
                    break;
                case 4:
                    System.out.println("ECC");
                    EnsembleOfClassifierChains ensembleOfClassifierChains = new EnsembleOfClassifierChains(new J48(), enSize, true, true);
                    results = eval.crossValidate(ensembleOfClassifierChains, dataSet, measures, nFolds);
                    metricBasisValues[es] = results.getMean(metricName);
                    System.out.println(results);
                    break;
                case 5:
                    System.out.println("ECC-DTa");
                    EnsembleOfClassifierChainsDT ensembleOfClassifierChainsDT = new EnsembleOfClassifierChainsDT(new J48(), enSize, true, true, MLDT.PredictionType.COMBINED);
                    results = eval.crossValidate(ensembleOfClassifierChainsDT, dataSet, measures, nFolds);
                    metricDeltaValues[es] = (results.getMean(metricName) - metricBasisValues[es]) / (metricBasisValues[es]);
                    System.out.println(results);
                    break;
                case 6:
                    System.out.println("ECC-DTb");
                    EnsembleOfClassifierChainsDT ensembleOfClassifierChainsDT2 = new EnsembleOfClassifierChainsDT(new J48(), enSize, true, true, MLDT.PredictionType.INDIVIDUAL);
                    results = eval.crossValidate(ensembleOfClassifierChainsDT2, dataSet, measures, nFolds);
                    metricDeltaValues[es] = (results.getMean(metricName) - metricBasisValues[es]) / (metricBasisValues[es]);
                    System.out.println(results);
                    break;
                case 7:
                    System.out.println("EPS");
                    EnsembleOfPrunedSets ensembleOfPrunedSets = new EnsembleOfPrunedSets(66, enSize, 0.5, 2, PrunedSets.Strategy.A, 3, new J48());
                    results = eval.crossValidate(ensembleOfPrunedSets, dataSet, measures, nFolds);
                    metricBasisValues[es] = results.getMean(metricName);
                    System.out.println(results);
                    break;
                case 8:
                    System.out.println("EPS-DTa");
                    EnsembleOfPrunedSetsDT ensembleOfPrunedSetsDT = new EnsembleOfPrunedSetsDT(66, enSize, 0.5, 2, PrunedSets.Strategy.A, 3, new J48(), MLDT.PredictionType.COMBINED);
                    results = eval.crossValidate(ensembleOfPrunedSetsDT, dataSet, measures, nFolds);
                    metricDeltaValues[es] = (results.getMean(metricName) - metricBasisValues[es]) / (metricBasisValues[es]);
                    System.out.println(results);
                    break;
                case 9:
                    System.out.println("EPS-DTb");
                    EnsembleOfPrunedSetsDT ensembleOfPrunedSetsDT2 = new EnsembleOfPrunedSetsDT(66, enSize, 0.5, 2, PrunedSets.Strategy.A, 3, new J48(), MLDT.PredictionType.INDIVIDUAL);
                    results = eval.crossValidate(ensembleOfPrunedSetsDT2, dataSet, measures, nFolds);
                    metricDeltaValues[es] = (results.getMean(metricName) - metricBasisValues[es]) / (metricBasisValues[es]);
                    System.out.println(results);
                    break;
                case 10:
                    System.out.println("ESL");
                    EnsembleOfSubsetLearners esl = new EnsembleOfSubsetLearners(new BinaryRelevance(new J48()), new J48(), new ConditionalDependenceIdentifier(new J48()), enSize);
                    results = eval.crossValidate(esl, dataSet, measures, nFolds);
                    metricBasisValues[es] = results.getMean(metricName);
                    metricDeltaValues[es] = (results.getMean(metricName) - metricBasisValues[es]) / (metricBasisValues[es]);
                    System.out.println(results);
                    break;
                case 11:
                    System.out.println("ESL DTa");
                    EnsembleOfSubsetLearnersDT esldt1 = new EnsembleOfSubsetLearnersDT(new BinaryRelevance(new J48()), new J48(), new ConditionalDependenceIdentifier(new J48()), enSize, MLDT.PredictionType.COMBINED);
                    results = eval.crossValidate(esldt1, dataSet, measures, nFolds);
                    metricDeltaValues[es] = (results.getMean(metricName) - metricBasisValues[es]) / (metricBasisValues[es]);
                    System.out.println(results);
                    break;
                case 12:
                    System.out.println("ESL DTb");
                    EnsembleOfSubsetLearnersDT esldt2 = new EnsembleOfSubsetLearnersDT(new BinaryRelevance(new J48()), new J48(), new ConditionalDependenceIdentifier(new J48()), enSize, MLDT.PredictionType.INDIVIDUAL);
                    results = eval.crossValidate(esldt2, dataSet, measures, nFolds);
                    metricDeltaValues[es] = (results.getMean(metricName) - metricBasisValues[es]) / (metricBasisValues[es]);
                    System.out.println(results);
                    break;
                default:
                    System.out.println("Invalid operation");
                    break;
            }

        } catch (InterruptedException ie) {
            System.out.println("Interruption " + ie);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws FileNotFoundException {
        int[] ensizes = {2, 4, 8, 16, 32, 64, 128, 256};
        metricBasisValues = new double[ensizes.length];
        metricDeltaValues = new double[ensizes.length];
        List<String> bases = new ArrayList<>();
        /*PROBLEMATIC DATABASES*/
//        bases.add("flags");
//        bases.add("emotions");
//        bases.add("birds");
//        bases.add("genbase");
//        bases.add("scene");
//        bases.add("genbase");//rapido
//        bases.add("enron");//demora
//        bases.add("CAL500");//demora
//        bases.add("medical");//demora
//        bases.add("bibtex");//infinito
//        bases.add("delicious");//problema
//        bases.add("rcv1subset1");//infinito
//        bases.add("Corel5k");//OOM
//        bases.add("tmc2007");
//        bases.add("mediamill");
//        bases.add("bookmarks");
//        bases.add("genbase");
//        bases.add("medical");
        //SMO com BR parece melhorar levemente
        for (String b : args) {
            TestesMultilabelDecisionTemplateES.args = args;
            basename = b;
            System.out.println(b);
            try {
                for (int i = 1; i <= 2; i++) {
                    for (es = 0; es < ensizes.length; es++) {
                        met = i;
                        enSize = ensizes[es];
                        System.out.println("es>" + es);
//                  
//                    if (i == 1 || i == 2) {
//                        continue;
//                    }
                        arqout = new PrintStream(new FileOutputStream(basename + ".out", true));
//                    System.setOut(arqout);
//                    System.setErr(arqout);
                        Thread t = new Thread(new TestesMultilabelDecisionTemplateES(), basename + " Thread");
                        t.start();
                        t.join();
                    }
                    System.out.println(Arrays.toString(metricDeltaValues));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public TechnicalInformation getTechnicalInformation() {
        TechnicalInformation result;

        result = new TechnicalInformation(Type.ARTICLE);
        result.setValue(Field.AUTHOR, "Victor Freitas Rocha");
        result.setValue(Field.TITLE, "PG 2016/2");
        result.setValue(Field.JOURNAL, "Pattern Recogn.");
        result.setValue(Field.VOLUME, "0");
        result.setValue(Field.NUMBER, "0");
        result.setValue(Field.YEAR, "2016");
        result.setValue(Field.ISSN, "0");
        result.setValue(Field.PAGES, "0");
        result.setValue(Field.PUBLISHER, "Ninfa");
        result.setValue(Field.ADDRESS, "Vitória, ES, Brasil");

        return result;
    }
}
