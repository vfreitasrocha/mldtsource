/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weka.classifiers.meta;

import java.util.ArrayList;
import weka.classifiers.Classifier;
import weka.classifiers.MultipleClassifiersCombiner;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.matrix.Matrix;

/**
 * <!-- globalinfo-start -->
 * Class for combining classifiers using the Decision Template mothod for
 * classification. The similarity between the Decision Profile and the Decision
 * Templates is computed using the squared Euclidean distance.
 * <br/>
 * For more information see:<br/>
 * <br/>
 * Ludmila I. Kuncheva (2004). Combining Pattern Classifiers: Methods and
 * Algorithms. Wiley.
 * <p/>
 * <!-- globalinfo-end -->
 *
 * <!-- options-start -->
 * Valid options are:
 * <p/>
 *
 * <pre> -B &lt;classifier specification&gt;
 *  Full class name of classifier to include, followed
 *  by scheme options. May be specified multiple times.
 *  (default: "weka.classifiers.rules.ZeroR")</pre>
 *
 * <pre> -D
 *  If set, classifier is run in debug mode and
 *  may output additional info to the console</pre>
 *
 *
 * <!-- options-end -->
 *
 * @author Roberto Perdisci (roberto.perdisci@gmail.com)
 * @version $Revision: 1.0 $
 */
public class DecisionTemplate extends MultipleClassifiersCombiner {

    /**
     * for serialization
     */
    static final long serialVersionUID = -637891196294399624L;

    private ArrayList templates;
    private int numOfClasses;
    private boolean buildeach = true;
    private int[] numOfInstancesPerClass;

    /*
     * buildeach = true, roda buildClassifier para cada classificador
     */
    public void setBuildEach(boolean set) {
        this.buildeach = set;
    }

    /**
     * Returns a string describing classifier
     *
     * @return a description suitable for displaying in the
     * explorer/experimenter gui
     */
    public String globalInfo() {

        return "Class for combining classifiers using the Decision Template method for classification.";
    }

    //[c1l1, c2l1, c3l1] ... [c1l2, c2l2, c3l2]
    //classValue = 1 se tem, classValue = 1 se nao tem
    public void addTsoDp(double[] distributionForInstance, int classValue) {

        if (m_Classifiers.length == 1) {
            m_Classifiers = new Classifier[distributionForInstance.length];
        }

        System.out.println("distributionForInstance.length: " + distributionForInstance.length);
        System.out.println("m_Classifiers.length: " + m_Classifiers.length);

        numOfClasses = 2;
        if (templates == null) {
            //Initializes the Decision Templates for each class
            templates = new ArrayList();
            for (int i = 0; i < numOfClasses; i++) {
                templates.add(new Matrix(m_Classifiers.length, numOfClasses));//Matrix[Numero de Classificadores][Numero de classes]
            }
            numOfInstancesPerClass = new int[numOfClasses];
        }

        Matrix decisionProfile = new Matrix(distributionForInstance.length, numOfClasses);

        // Compute the Decision Profile, DP(x)
        for (int j = 0; j < m_Classifiers.length; j++) {
            double[] dfi = new double[2];
            dfi[0] = distributionForInstance[j];
            dfi[1] = 1 - distributionForInstance[j];

            setRow(decisionProfile, j, dfi/*distributionForInstance*/);
        }

        // The Decision Profiles DP(x) is added to the Decision Profiles of instances
        // that have a class value equal to the class of x
        Matrix update = ((Matrix) templates.get((int) classValue)).plus(decisionProfile);
        templates.set((int) classValue, update);
        numOfInstancesPerClass[classValue]++;
    }

    //depois de gerar distribuições para todas as instancias
    public void creatDts() {
        // Computes the Decision Templates by averaging with respect to the total number of Instances per class
        for (int k = 0; k < numOfClasses; k++) {
            Matrix avg = (((Matrix) templates.get(k)).times(1 / (double) numOfInstancesPerClass[k]));
            templates.set(k, avg);
        }
    }

    /**
     * Buildclassifier selects a classifier from the set of classifiers by
     * minimising error on the training data.
     *
     * @param data the training data to be used for generating the boosted
     * classifier.
     * @throws Exception if the classifier could not be built successfully
     */
    public void buildClassifier(Instances data) throws Exception {

        // can classifier handle the data?
        getCapabilities().testWithFail(data);

        // remove instances with missing class
        Instances newData = new Instances(data);
        newData.deleteWithMissingClass();

        for (int i = 0; buildeach && i < m_Classifiers.length; i++) {
            getClassifier(i).buildClassifier(data);
        }

        // Stores the number of classes
        numOfClasses = newData.numClasses();
        if (numOfClasses <= 1) {
            throw new Exception("DecisionTemplate can be only used for classification!");
        }

        //Initializes the Decision Templates for each class
        templates = new ArrayList();
        for (int i = 0; i < numOfClasses; i++) {
            templates.add(new Matrix(m_Classifiers.length, numOfClasses));//Matrix[Numero de Classificadores][Numero de classes]
        }

        // Sums the Decision Profile DP(x) computed for each instance x in the dataset
        int numOfInstances = newData.numInstances();
        int[] numOfInstancesPerClass = new int[numOfClasses];
        for (int i = 0; i < numOfInstances; i++) {
            Matrix decisionProfile = new Matrix(m_Classifiers.length, numOfClasses);
            Instance x = newData.instance(i);

            // Compute the Decision Profile, DP(x)
            for (int j = 0; j < m_Classifiers.length; j++) {
//                System.out.println(j+">"+Arrays.toString(getClassifier(j).distributionForInstance(x)));
                setRow(decisionProfile, j, getClassifier(j).distributionForInstance(x));
            }

            // The Decision Profiles DP(x) is added to the Decision Profiles of instances
            // that have a class value equal to the class of x
            Matrix update = ((Matrix) templates.get((int) x.classValue())).plus(decisionProfile);
            templates.set((int) x.classValue(), update);
            numOfInstancesPerClass[(int) x.classValue()]++;
        }

        // Computes the Decision Templates by averaging with respect to the total number of Instances per class
        for (int k = 0; k < numOfClasses; k++) {
            Matrix avg = (((Matrix) templates.get(k)).times(1 / (double) numOfInstancesPerClass[k]));
            templates.set(k, avg);
        }
    }

    /**
     * Classifies the given test instance.
     *
     * @param instance the instance to be classified
     * @return the predicted class for the instance
     * @throws Exception if the instance can't be classified
     */
    public double[] distributionForInstance(Instance instance) throws Exception {

        double[] dist = new double[numOfClasses];

        // Computes the Decision Profile for the given instance
        Matrix decisionProfile = new Matrix(m_Classifiers.length, numOfClasses);
        for (int j = 0; j < m_Classifiers.length; j++) {
            setRow(decisionProfile, j, getClassifier(j).distributionForInstance(instance));
        }

        // Computes the similarity between DP and DTj for each class j=0,..,numOfClasses
        for (int i = 0; i < numOfClasses; i++) {
            dist[i] = templateSimilarity((Matrix) templates.get(i), decisionProfile);
        }

        return dist;
    }

    /**
     * Classifies the given test instance.
     *
     * @param instance the instance to be classified
     * @return the predicted class for the instance
     * @throws Exception if the instance can't be classified
     */
    public double[] distributionForInstance2(double[][] distributions) throws Exception {

        double[] dist = new double[numOfClasses];

        if (m_Classifiers.length != distributions.length) {
            throw new ArrayIndexOutOfBoundsException(m_Classifiers.length);
        }

        // Computes the Decision Profile for the given instance
        Matrix decisionProfile = new Matrix(m_Classifiers.length, numOfClasses);
        for (int j = 0; j < m_Classifiers.length; j++) {
            setRow(decisionProfile, j, distributions[j]);
        }

        // Computes the similarity between DP and DTj for each class j=0,..,numOfClasses
        for (int i = 0; i < numOfClasses; i++) {
            dist[i] = templateSimilarity((Matrix) templates.get(i), decisionProfile);
        }

        return dist;
    }

    /**
     * Computes the similarity between two matrixes using the squared Euclidean
     * distance. <br>
     * For more information see: <br>
     * Ludmila I. Kuncheva (2004). Combining Pattern Classifiers: Methods and
     * Algorithms. Wiley.<br>
     * <br>
     * The two matrices must have the same dimension. <br>
     *
     * @param m1 a NxM matrix
     * @param m2 a NxM matrix
     * @return the similarity between the two matrices
     */
    private double templateSimilarity(Matrix m1, Matrix m2) {

        double dist = 0;
        for (int i = 0; i < m_Classifiers.length; i++) {
            for (int k = 0; k < numOfClasses; k++) {
                dist += Math.pow(m1.get(i, k) - m2.get(i, k), 2);
            }
        }

        // The similarity is equal to 1 minus the average squared Euclidean distance
        return 1 - dist / (m_Classifiers.length * numOfClasses);
    }

    /**
     * Sets a row of a matrix
     *
     * @param m a Matrix
     * @param i the row index
     * @param s the row to be set
     */
    private void setRow(Matrix m, int i, double[] s) {
        for (int j = 0; j < s.length; j++) {
            m.set(i, j, s[j]);
        }
    }

    /**
     * Output a representation of this classifier
     *
     * @return a string representation of the classifier
     */
    public String toString() {

        if (m_Classifiers == null) {
            return "DecisionTempate: No model built yet.";
        }

        String result = "DevisionTemplate combines";
        result += " the output of these base learners:\n";
        for (int i = 0; i < m_Classifiers.length; i++) {
            result += '\t' + getClassifierSpec(i) + '\n';
        }

        return result;
    }

    /**
     * Main method for testing this class.
     *
     * @param argv should contain the following arguments: -t training file [-T
     * test file] [-c class index]
     */
    public static void main(String[] argv) {
        runClassifier(new DecisionTemplate(), argv);
    }
}
